/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zshanabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/19 21:46:58 by zshanabe          #+#    #+#             */
/*   Updated: 2018/03/20 17:52:15 by zshanabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int		len;
	int		*arr;
	int		i;

	i = 0;
	len = max - min;
	if (len <= 0)
		return (0);
	arr = malloc(sizeof(int) * len);
	if (arr == NULL)
		return (0);
	while (i < len)
	{
		arr[i] = min + i;
		i++;
	}
	return (arr);
}
